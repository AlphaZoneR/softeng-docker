const mongoose = require('mongoose');
const password = process.env.MONGODB_PASSWORD || '';

const connectionString = `mongodb+srv://split:${password}@afim1689-softeng-iqyjq.gcp.mongodb.net/test?retryWrites=true&w=majority`;
mongoose.connect(connectionString, {useNewUrlParser: true, autoReconnect: true});

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        unique: true
    }
});

module.exports = mongoose.model('User', userSchema);
