var express = require('express');
var router = express.Router();
var {userModel} = require('../database');

/* GET users listing. */
router.get('/',  async function(req, res, next) {
  const users = await userModel.find({});
  console.log(users);
  res.render('users', { users: users });
});

module.exports = router;