FROM node:12.13.0-alpine
WORKDIR /webapp
COPY ./ ./
RUN npm install
CMD ["node", "/webapp/bin/www"]
EXPOSE 3000